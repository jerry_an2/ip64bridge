# IP64Bridge: A TCP/UDP packet forwarding tool for IPv6 and IPv4

Use this program to enable IPv6 remote multiplay for games that support only
IPv4 or local LAN connections. It is also possible to customize the port
forwarding/mapping rules.

## Quickstart

### Server configuration

Prerequisites:
* A publicly accessable IPv6 address
* Firewall rules that allow incoming TCP/UDP traffic on the selected port

Steps:
1. Select the "**Host and play**" mode
2. Select TCP and/or UDP to match the protocol(s) used by the game (select both
if unsure)
3. Select any source port to be used as the host IPv6 port
4. Set the destination port to match the port used by the game (search online
if unsure)
5. Click "**Connect**" to start forwarding packets
6. Send IPv6 address and port of host to clients

![Server config screenshot](doc/server_config_gui.png)

Alternatively, run `ip64bridge -h` or `python3 python/server.py -h` to see how
to run this program from command line.

### Client configuration

Prerequisites:
* Enabled IPv6 support in OS & router

Steps:
1. Select the "**Join via IP**" mode
2. Select TCP and/or UDP to match the protocol(s) used by the game (select both
if unsure)
3. Set the source port to match the port used by the game (search online if
unsure)
4. Enter host IPv6 address/port in the destination address/port fields
(support for domain names added in v1.0.0)
5. Click "**Connect**" to start forwarding packets

If using a domain name as the destination, please be aware that it would only be
resolved once in the beginning. The app would not know about future DNS record
updates.

![Client config screenshot](doc/client_config_gui.png)

Alternatively, run `ip64bridge -h` or `python3 python/client.py -h` to see how
to run this program from command line.

### Config file

Support for config files has been added in v0.3.0.
Config files are in json format, like the example below:

```
{
    "verbosity": 1,
    "gui": false,
    "bridges": [
        {
            "name": "example1",
            "src": "[::]:8888",
            "dst": "127.0.0.1:9999",
            "tcp": true,
            "udp": false
        },
        {
            "name": "example2",
            "src": "0.0.0.0:7777",
            "dst": "[::1]:6666",
            "tcp": true,
            "udp": true
        }
    ]
}
```
Usage: `python3 python/main.py -f config.json`, or `ip64bridge -f config.json`

# Roadmap

All features I initially planned have been added as of v1.0.0. Next I will focus
on improving user experience

- [ ] Display helpful messages for both user errors and app bugs
- [ ] (Maybe) a port scanner function to easily find ports used by an app
- [ ] Various customization configs
- [ ] Proper logging (to make debugging easier for me)
