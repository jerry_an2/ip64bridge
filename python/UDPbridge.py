import socket
import logging
import traceback
# from multiprocessing import Process, Event, current_process, Manager
from threading import Thread, Event

from common import simple_forward, Bridge, str2addr

class UDPbridge(Bridge):
    def __init__(self, srcType=socket.AF_INET, srcPort=7777, buf_size=2048, dstType=socket.AF_INET, dstPort=7778, dstAddr="127.0.0.1") -> None:
        super().__init__(srcType, srcPort, buf_size, dstType, dstPort, dstAddr)
        # Initialize the source socket (destination sockets would be created on demand)
        # UDP uses socket kind SOCK_DGRAM
        self.srcSock = socket.socket(srcType, socket.SOCK_DGRAM)
        # The source socket is where the external data packets would arrive at
        self.srcSock.bind(("", srcPort))
        # Pick a random free port to bind with midSock.
        # The midSocks are responsible for sending data to the actual destination
        # They will be created when a new connection is made

    @staticmethod
    def from_str(src: str, dst: str, buf_size=2048):
        srcType, srcAddr, srcPort = str2addr(src)
        dstType, dstAddr, dstPort = str2addr(dst)
        return UDPbridge(srcType, srcPort, buf_size, dstType, dstPort, dstAddr)

    def udp_response_forward(self, remote, event):
        # Add PID of current process to parent's collection,
        # allowing parent to terminate this process if needed
        try:
            simple_forward(self.midSock[remote], self.srcSock, remote, event, self.BUF_SIZE)
        except Exception as e:
            logging.error(e)
        # On completion, delete sock from dictionary
        self.midSock[remote].close()
        del self.midSock[remote]
        # del self.rspPID[remote]
        logging.warning(f"The remote {remote} is no longer active")

    def __run(self):
        '''
        This function does source -> destination forwarding and spawns child processes to handle responses
        '''
        print(f"Mapped local port {self.srcPort} to {self.dstAddr}:{self.dstPort}")
        while True:
            try:
                # 1. Listen to the source socket for any incoming packets
                recvData, remote = self.srcSock.recvfrom(self.BUF_SIZE)
                logging.debug(f"Received {len(recvData)} bytes of data on port {self.srcPort} from {remote}")
                # 2. If remote has changed, Start a new process that listens to local response (on midSock)
                # and forwards that back to remote
                if remote not in self.midSock:
                    logging.warning(f"New UDP `connection` from {remote}.")
                    # Unlike TCP, UDP does not have real "connections"
                    # But we do need to maintain a unique socket for each remote host
                    self.midSock[remote] = socket.socket(self.dstType, socket.SOCK_DGRAM)
                    self.midSock[remote].bind(("", 0))
                    self.midSock[remote].settimeout(30) # Remove the socket if it has been inactive for too long
                    event = Event()
                    rspProc = Thread(target=self.udp_response_forward, args=(remote, event), daemon=True)
                    rspProc.start()
                    # When the connection closes, the process should end and delete corresponding socket from dictionary
                    event.wait()
                # 3. The response-handling process should be up now, so we may forward the received traffic
                sentLen = self.midSock[remote].sendto(recvData, (self.dstAddr, self.dstPort))
                assert(len(recvData) == sentLen)
            except (TimeoutError, ConnectionResetError) as e:
                # These are known harmless exceptions, so execution may continue
                logging.debug(e)

    def run(self):
        '''
        A wrapper of the main __run function that attemps to gracefully shutdown on exit
        '''
        try:
            self.__run()
        except KeyboardInterrupt:
            logging.critical("Process terminated by KeyboardInterrupt")
        except Exception as e:
            logging.critical(traceback.format_exc())
        # On exit, close all sockets and kill child processes
        logging.warning(f"Closing main socket")
        self.srcSock.close()
        logging.warning(f"Closing {len(self.midSock)} temporary sockets")
        for sock in self.midSock.values(): sock.close()

if __name__ == "__main__":
    logging.basicConfig(level=logging.CRITICAL)
    bridge = UDPbridge(srcType=socket.AF_INET6, dstPort=7777)
    bridge.run()
