import socket
from ipaddress import ip_address, IPv4Address, IPv6Address
import logging
import traceback

def simple_forward(src: socket.socket, dst: socket.socket, remote = None, event = None, BUF_SIZE=2048):
    if (event): event.set()
    while True:
        try:
            response = src.recv(BUF_SIZE)
            # If remote address is given, assume UDP, else assume TCP
            sentLen = dst.sendto(response, remote) if (remote) else dst.send(response)
            assert(len(response) == sentLen)
        except (ConnectionResetError, ConnectionAbortedError, TimeoutError) as e:
            logging.info(traceback.format_exc())
            break

def str2addr(raw: str) -> (socket.AddressFamily, str, int):
    segments = raw.split(":")
    port = int(segments.pop())
    addr = ':'.join(segments).replace('[', '').replace(']', '')
    ip = ip_address(addr)
    family = socket.AF_INET6 if type(ip) == IPv6Address else socket.AF_INET
    return (family, addr, port)

class Bridge:
    def __init__(self, srcType = socket.AF_INET, srcPort = 7777, buf_size = 2048,
                 dstType = socket.AF_INET, dstPort = 7778, dstAddr = "127.0.0.1") -> None:
        # Save the address & ports for future use
        self.BUF_SIZE = buf_size
        self.srcType = srcType
        self.srcPort = srcPort
        self.dstType = dstType
        self.dstPort = dstPort
        self.dstAddr = dstAddr
        # Using a manager to create shared dicts (which will be modified in child processes)
        # manager = Manager()
        self.midSock = {}

    def run(self):
        pass
