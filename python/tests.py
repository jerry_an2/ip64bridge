import socket
from multiprocessing import Process, Event
import logging
import time
import random

def run_UDP_server(port = 7777, type = socket.AF_INET, event = Event(), total = -1):
    logging.basicConfig(level=logging.DEBUG)
    logging.info("Server process started")
    event.set()
    sock = socket.socket(type, socket.SOCK_DGRAM)
    sock.bind(("", port))
    sock.settimeout(10)
    count = 0
    while count != total:
        try: 
            recvData, remote = sock.recvfrom(2048)
            logging.debug(f"Server received {len(recvData)} bytes of data from {remote}")
            logging.debug(f"Server received: {recvData}")
            count += 1
            logging.debug(f"Current count: {count}")
            sock.sendto(recvData, remote)
        except Exception as e:
            logging.critical(f"Got exception {e}")
            sock.close()
            break
    logging.warning(f"All packets received, current time {time.time()}")

def run_UDP_client(serverIP = "127.0.0.1", serverPort = 7777, serverType = socket.AF_INET, repeat = -1):
    sock = socket.socket(serverType, socket.SOCK_DGRAM)
    sock.settimeout(5)
    data = b"Hello there!"
    totalDelay = 0
    success = 0
    if (repeat == -1): repeat = random.randint(5,20)
    for _ in range(repeat):
        startTime = time.time()
        try:
            sendLen = sock.sendto(data, (serverIP, serverPort))
        except PermissionError:
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            sendLen = sock.sendto(data, (serverIP, serverPort))
        assert(sendLen == len(data))
        try:
            recv = sock.recv(2048)
            assert(recv == data)
            stopTime = time.time()
            totalDelay += stopTime-startTime
            success += 1
        except TimeoutError as e:
            logging.debug("No response from server was observer. Sending a new packet")
    logging.info(f"{repeat} packets sent. Average roundtrip time: {totalDelay*1000/success}ms")
    sock.close()
    return repeat

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    startEvent = Event()
    # Run server process in the background
    serverProc = Process(target=run_UDP_server, args=(7777, socket.AF_INET, startEvent))
    serverProc.start()
    startEvent.wait()
    # Client process in the foreground
    count = 0
    for _ in range(32):
        count += run_UDP_client(serverIP="::1", serverPort=7777, serverType=socket.AF_INET6)
    logging.warning(f"All packets sent, current time {time.time()}")
    time.sleep(10)
    logging.info(f"All tests completed. Client sent {count} chunks of data")
    serverProc.kill()
