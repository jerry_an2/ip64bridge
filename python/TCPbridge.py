import socket
import logging
import traceback
import sys
import os
import signal
# from multiprocessing import Process, Event, current_process
from threading import Thread, Event

from common import simple_forward, Bridge, str2addr

class TCPbridge(Bridge):
    def __init__(self, srcType=socket.AF_INET, srcPort=7777, buf_size=2048, dstType=socket.AF_INET, dstPort=7778, dstAddr="127.0.0.1") -> None:
        super().__init__(srcType, srcPort, buf_size, dstType, dstPort, dstAddr)
        # Initialize the source socket (destination sockets would be created on demand)
        # TCP uses socket kind SOCK_STREAM
        self.srcSock = socket.socket(srcType, socket.SOCK_STREAM)
        # The source socket is where the external data packets would arrive at
        self.srcSock.bind(("", srcPort))

    @staticmethod
    def from_str(src: str, dst: str, buf_size=2048):
        srcType, srcAddr, srcPort = str2addr(src)
        dstType, dstAddr, dstPort = str2addr(dst)
        return TCPbridge(srcType, srcPort, buf_size, dstType, dstPort, dstAddr)

    def tcp_request_forward(self, remote, event):
        # Add PID of current process to parent's collection,
        # allowing parent to terminate this process if needed
        reqSrc, reqDst = self.midSock[remote]
        try:
            simple_forward(reqSrc, reqDst, BUF_SIZE=self.BUF_SIZE)
        except Exception as e:
            logging.error(e)
        # We'll have the request forward process delete both sockets on exit
        reqSrc.close()
        reqDst.close()
        del self.midSock[remote]
        logging.warning(f"The remote {remote} is no longer active")

    def tcp_response_forward(self, remote, event):
        # Add PID of current process to parent's collection,
        # allowing parent to terminate this process if needed
        rspDst, rspSrc = self.midSock[remote]
        try:
            simple_forward(rspSrc, rspDst, BUF_SIZE=self.BUF_SIZE)
        except Exception as e:
            logging.error(e)
        # We'll have the request forward process delete both sockets on exit

    def __run(self):
        print(f"Mapped local port {self.srcPort} to {self.dstAddr}:{self.dstPort}")
        self.srcSock.listen()
        while True:
            try:
                # socket accept() returns:
                #   - a socket, which will be used to receive incoming data
                #   - an address, which will be used to identify the handler processes and sockets
                sock, remote = self.srcSock.accept()
                if remote in self.midSock:
                    logging.error(f"Remote {remote} already has an active connection")
                    # Consider terminating the process here
                else:
                    logging.info(f"New TCP connection from {remote}.")
                # bridgeSock is used to send data received on source socket to destination
                bridgeSock = socket.socket(self.dstType, socket.SOCK_STREAM)
                bridgeSock.connect((self.dstAddr, self.dstPort))
                self.midSock[remote] = (sock, bridgeSock)
                # Start processes to handle src -> dst and dst -> src traffic
                reqProc = Thread(target=self.tcp_request_forward, args=(remote, None), daemon=True)
                rspProc = Thread(target=self.tcp_response_forward, args=(remote, None), daemon=True)
                reqProc.start()
                rspProc.start()
            except (TimeoutError, ConnectionResetError) as e:
                # These are known harmless exceptions, so execution may continue
                logging.debug(e)

    def run(self):
        '''
        A wrapper of the main __run function that attemps to gracefully shutdown on exit
        '''
        try:
            self.__run()
        except KeyboardInterrupt:
            logging.critical("Process terminated by KeyboardInterrupt")
        except Exception as e:
            logging.critical(traceback.format_exc())
        # On exit, close all sockets and kill child processes
        logging.warning(f"Closing main socket")
        self.srcSock.close()
        logging.warning(f"Closing {2*len(self.midSock)} temporary sockets")
        for sock0, sock1 in self.midSock.values():
            sock0.close()
            sock1.close()
