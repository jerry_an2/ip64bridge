from UDPbridge import UDPbridge
from TCPbridge import TCPbridge
import socket
import argparse
from multiprocessing import Process
import logging
from info import VERSION

class Server:
    def __init__(self, srcPort=12345, dstPort=7777, udp=True, tcp=True) -> None:
        self.processes = []
        if udp or not tcp:
            bridge = UDPbridge(srcType=socket.AF_INET6, srcPort=srcPort,
                               dstType=socket.AF_INET, dstPort=dstPort, dstAddr="127.0.0.1")
            self.processes.append(Process(target=bridge.run, daemon=True))
        if tcp or not udp:
            bridge = TCPbridge(srcType=socket.AF_INET6, srcPort=srcPort,
                               dstType=socket.AF_INET, dstPort=dstPort, dstAddr="127.0.0.1")
            self.processes.append(Process(target=bridge.run, daemon=True))
        
    def run(self):
        for process in self.processes:
            process.start()
        while True:
            cmd = input(">>>")
            if cmd in ["q", "quit", "exit"]:
                break

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--src", type=int, default=54321, help="Public IPv6 port")
    parser.add_argument("-d", "--dst", type=int, default=7777, help="Local server port")
    parser.add_argument("-u", "--udp", action="store_true", help="Enable UDP traffic forwarding")
    parser.add_argument("-t", "--tcp", action="store_true", help="Enable TCP traffic forwarding")
    parser.add_argument("-v", "--version", action='version', version=f"ip64bridge_py {VERSION}")
    args = parser.parse_args()
    logging.basicConfig(level=logging.INFO)
    Server(args.src, args.dst, args.udp, args.tcp).run()
