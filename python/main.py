from UDPbridge import UDPbridge
from TCPbridge import TCPbridge
import socket
import argparse
from multiprocessing import Process
import logging
import json
from info import VERSION

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--src", help="Source <addr:port>")
    parser.add_argument("-d", "--dst", help="Destination <addr:port>")
    parser.add_argument("-u", "--udp", action="store_true", help="Enable UDP traffic forwarding")
    parser.add_argument("-t", "--tcp", action="store_true", help="Enable TCP traffic forwarding")
    parser.add_argument("-f", "--cfgfile", type=argparse.FileType("r"), help="Load config from file")
    parser.add_argument("-v", "--version", action='version', version=f"ip64bridge_py {VERSION}")
    args = parser.parse_args()

    bridges = []
    if args.cfgfile:
        config = json.load(args.cfgfile)
        for bridgeCfg in config["bridges"]:
            if (bridgeCfg["udp"]):
                bridges.append(UDPbridge.from_str(bridgeCfg["src"], bridgeCfg["dst"]))
            if (bridgeCfg["tcp"]):
                bridges.append(TCPbridge.from_str(bridgeCfg["src"], bridgeCfg["dst"]))
    if args.src and args.dst:
        pass #TODO

    for bridge in bridges:
            Process(target=bridge.run, daemon=True).start()
    while True:
        cmd = input(">>>")
        if cmd in ["q", "quit", "exit"]:
            break