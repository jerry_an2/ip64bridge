from UDPbridge import UDPbridge
from TCPbridge import TCPbridge
import socket
import argparse
from multiprocessing import Process
import logging
from info import VERSION

class Client:
    def __init__(self, srcPort=7777, dstPort=12345, dstAddr="::1", udp=True, tcp=True) -> None:
        self.processes = []
        if udp or not tcp:
            bridge = UDPbridge(srcType=socket.AF_INET, srcPort=srcPort,
                               dstType=socket.AF_INET6, dstPort=dstPort, dstAddr=dstAddr)
            self.processes.append(Process(target=bridge.run, daemon=True))
        if tcp or not udp:
            bridge = TCPbridge(srcType=socket.AF_INET, srcPort=srcPort,
                               dstType=socket.AF_INET6, dstPort=dstPort, dstAddr=dstAddr)
            self.processes.append(Process(target=bridge.run, daemon=True))

    def run(self):
        for process in self.processes:
            process.start()
        while True:
            cmd = input(">>>")
            if cmd in ["q", "quit", "exit"]:
                break

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("server_ip", help="Public IPv6 address of server")
    parser.add_argument("-s", "--src", type=int, default=7777, help="Local server port")
    parser.add_argument("-d", "--dst", type=int, default=54321, help="Public IPv6 port")
    parser.add_argument("-u", "--udp", action="store_true", help="Enable UDP traffic forwarding")
    parser.add_argument("-t", "--tcp", action="store_true", help="Enable TCP traffic forwarding")
    parser.add_argument("-v", "--version", action='version', version=f"ip64bridge_py {VERSION}")
    args = parser.parse_args()
    Client(args.src, args.dst, args.server_ip, args.udp, args.tcp).run()
