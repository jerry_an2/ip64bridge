#ifndef BRIDGETHREAD_H
#define BRIDGETHREAD_H

#include <QThread>
#include <QHostAddress>
#include "basebridge.h"

/**
 * @brief The BridgeThread class
 *
 * This class, inherited from QThread, allows starting a UDP or TCP bridge
 * easily in a thread.
 */
class BridgeThread : public QThread
{
    Q_OBJECT
public:
    BaseBridge *bridge;
    QHostAddress srcAddr;
    quint16 srcPort;
    QHostAddress dstAddr;
    quint16 dstPort;
    QString name;
    int connectionCount;
    explicit BridgeThread(BaseBridge *bridge, QObject *parent = nullptr);

    // QThread interface
    int getConnectionCount() const;
    void setConnectionCount(int newConnectionCount);

protected:
    void run();
};

#endif // BRIDGETHREAD_H
