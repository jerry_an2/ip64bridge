#include "bridgelistitem.h"

BridgeListItem::BridgeListItem(QListWidget *listview, int type) : QListWidgetItem(listview, type)
{}

BridgeThread *BridgeListItem::getBThread() const
{
    return bThread;
}

void BridgeListItem::setBThread(BridgeThread *newBThread)
{
    bThread = newBThread;
    setText(bThread->name);
}
