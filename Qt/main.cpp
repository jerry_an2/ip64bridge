#include "mainwindow.h"

#include <QApplication>
#include <QDebug>
#include <QLoggingCategory>
#include <QCommandLineParser>
#include <QUrl>
#include <QDir>
#include <QStandardPaths>
#include "qjsonobject.h"
#include "udpbridge.h"
#include "tcpbridge.h"
#include <iostream>
#include <QNetworkProxy>

/**
 * @brief set_log_level
 * @param level
 *
 * Disable logging of all messages with lower log level than <level>
 */
void set_log_level(int level) {
    // Note the fall-thru. The missing "break" in each case is intended.
    switch (level) {
        case 0:
            QLoggingCategory::defaultCategory()->setEnabled(QtCriticalMsg, false);
        case 1:
            QLoggingCategory::defaultCategory()->setEnabled(QtWarningMsg, false);
        case 2:
            QLoggingCategory::defaultCategory()->setEnabled(QtInfoMsg, false);
        case 3:
            QLoggingCategory::defaultCategory()->setEnabled(QtDebugMsg, false);
        default:
            break;
    }
}

/**
 * @brief headless_main
 * @param parser - Reference of the parser object
 * @param w - Reference of the main window object
 * @return Number of bridges started
 *
 * This program would first try to run in non-gui mode, but if not enough
 * arguments are supplied to create any bridge, the GUI would be started
 * after returning from this function.
 */
int headless_main(QCommandLineParser &parser, MainWindow &w) {
    // If either source or destination was not provided on the command line,
    // return 0 so the GUI can be started
    if (parser.positionalArguments().size() < 2) return 0;
    HostIdentity src(parser.positionalArguments().at(0));
    HostIdentity dst(parser.positionalArguments().at(1));
    bool tcp = parser.isSet("tcp");
    bool udp = parser.isSet("udp");
    std::cout << "SRC: " << src.toString().toLocal8Bit().data() << std::endl;
    std::cout << "DST: " << dst.toString().toLocal8Bit().data() << std::endl;
    if (!src.isValid() || !dst.isValid()) return 0;
    if (!tcp && !udp) {
        // We'll treat this configuration as "dry-run"
        // i.e., display parsing result and return -1.
        std::cout << "Specify '-t'/'-u' to start a TCP/UDP bridge (or both)" << std::endl;
        return -1;
    }
    QHostInfo::lookupHost(dst.addr, [&](const QHostInfo &result){
        QList<QHostAddress> ipList = result.addresses();
        if (ipList.isEmpty()) return;
        std::sort(ipList.begin(), ipList.end());
        if (tcp) {
            TcpBridge *bridge = new TcpBridge(QHostAddress(src.addr), src.port, ipList[0], dst.port);
            bridge->dstDomain = dst.addr;
            w.start_bridge_thread(bridge);
        }
        if (udp) {
            UdpBridge *bridge = new UdpBridge(QHostAddress(src.addr), src.port, ipList[0], dst.port);
            bridge->dstDomain = dst.addr;
            w.start_bridge_thread(bridge);
        }
    });
    return tcp + udp;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    QApplication::setApplicationVersion("1.0.0");
    // System proxy might break this app, so we disable it
    QNetworkProxyFactory::setUseSystemConfiguration(false);
#ifdef QT_DEBUG
    set_log_level(4);
#else
    set_log_level(3);
#endif
    QDir cfgDir(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation));
    if (!cfgDir.exists()) cfgDir.mkpath(".");
    // Parse command line options
    QCommandLineParser parser;
    QCommandLineOption tcpOption({"t", "tcp"}, a.tr("Create TCP bridge."));
    QCommandLineOption udpOption({"u", "udp"}, a.tr("Create UDP bridge."));
    QCommandLineOption logOption({"V", "verbosity"}, a.tr("Set verbosity/log level (0-4)"), "verbosity");
    QCommandLineOption cfgOption({"f", "cfgfile"}, a.tr("Load JSON config file"), a.tr("file"), cfgDir.filePath("config.json"));
    // QCommandLineOption srcOption({"s", "src", "source"}, a.tr("Source address and port"), "addr:port");
    // QCommandLineOption dstOption({"d", "dst", "destination"}, a.tr("Destination address and port"), "addr:port");
    parser.addPositionalArgument("source", "Source <address:port>");
    parser.addPositionalArgument("destination", "Destination <address:port>");
    parser.addOptions({tcpOption, udpOption, logOption, cfgOption});
    parser.addHelpOption();
    parser.addVersionOption();
    parser.process(a);
    // Override default verbosity if a valid one is given
    bool ok;
    int verbosity = parser.value(logOption).toInt(&ok);
    if (ok && verbosity >= 0) set_log_level(verbosity);
    QJsonObject cfgParseResult;
    w.load_config(parser.value(cfgOption), &cfgParseResult);
    // Start GUI if no bridge is started in headless mode
    if (headless_main(parser, w) == 0 && cfgParseResult.value("gui").toBool(true)) w.show();
    std::cout << "Enter event loop" << std::endl;
    return a.exec();
}
