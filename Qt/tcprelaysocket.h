#ifndef TCPRELAYSOCKET_H
#define TCPRELAYSOCKET_H

#include <QTcpSocket>

/**
 * @brief The TcpRelaySocket class
 *
 * Each instance of this class is tied to a TCP connection.
 * Incoming data would be read from srcSock and sent to destination via self.
 * Response data would be read from self and sent back to source via srcSock.
 */
class TcpRelaySocket : public QTcpSocket
{
    Q_OBJECT
public:
    QTcpSocket *srcSock;
    // srcAddr and srcPort are for logging purposes only
    QHostAddress srcAddr;
    quint16 srcPort;
    explicit TcpRelaySocket(QTcpSocket *srcSock, QObject *parent = nullptr);
    ~TcpRelaySocket();
    virtual void forward_request();
    virtual void forward_response();
};

#endif // TCPRELAYSOCKET_H
