#include "tcprelaysocket.h"

TcpRelaySocket::TcpRelaySocket(QTcpSocket *srcSock, QObject *parent)
    : QTcpSocket{parent}, srcSock(srcSock)
{
    // Unlike UdpRelaySocket where srcSock is shared,
    // srcSock in TcpRelaySocket is deidcated to this connection
    srcSock->setParent(this);
    srcAddr = srcSock->peerAddress();
    srcPort = srcSock->peerPort();
    // Set-up signal-slot connections for data forwarding
    connect(srcSock, &QTcpSocket::readyRead, this, &TcpRelaySocket::forward_request);
    connect(this, &QTcpSocket::readyRead, this, &TcpRelaySocket::forward_response);
    // When the source closes connection with srcSock, connection with destination
    // should also be closed
    connect(srcSock, &QTcpSocket::disconnected, this, &QTcpSocket::disconnectFromHost);
}

TcpRelaySocket::~TcpRelaySocket()
{
    qInfo() << QString("Disconnected from source %1:%2/tcp").arg(
        srcAddr.toString(), QString::number(srcPort)
        );
}

/**
 * @brief TcpRelaySocket::forward_request
 *
 * Read data from srcSock and write to dstSock (this)
 */
void TcpRelaySocket::forward_request()
{
    QByteArray data = srcSock->readAll();
    while (data.size() > 0) {
        write(data);
        data = srcSock->readAll();
    }
}

/**
 * @brief TcpRelaySocket::forward_response
 *
 * Read data from dstSock (this) and write to srcSock
 */
void TcpRelaySocket::forward_response()
{
    QByteArray data = readAll();
    while (data.size() > 0) {
        srcSock->write(data);
        data = readAll();
    }
}
