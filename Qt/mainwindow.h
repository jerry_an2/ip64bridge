#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QClipboard>
#include <QTimer>
#include <QDir>
#include <QStandardPaths>
#include "basebridge.h"
#include <QHostInfo>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QClipboard *clipboard;
    QTimer *timer;
    QDir cfgDir;
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    /**
     * @brief set_xxx_defaults
     *
     * Fill address:port of source and destination with default vaules
     */
    void set_host_defaults();
    void set_join_defaults();
    void set_custom_defaults();
    void swap_ports();

    // Bridge management
    void create_bridge();
    void start_bridge_thread(BaseBridge *bridge);
    void update_bridge_status();
    void terminate_selected();
    void terminate_all();
    bool load_config(QString fileName, QJsonObject* cfgObj = nullptr);
    bool save_config(QString fileName);
    void load_config_select();
    void save_config_select();

    // Get IP of host
    void get_ip_suggestions();
    void copy_ipv6();

    // System tray icon
    void create_tray_icon();

private:
    Ui::MainWindow *ui;

    // QWidget interface
protected:
    void closeEvent(QCloseEvent *event);
};

bool operator<(const QHostAddress &lhs, const QHostAddress &rhs) noexcept;

#endif // MAINWINDOW_H
