#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "bridgelistitem.h"
#include "udpbridge.h"
#include "tcpbridge.h"
#include <QNetworkInterface>
#include <QSystemTrayIcon>
#include <QCloseEvent>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    clipboard = QApplication::clipboard();

    // Default config dir
    cfgDir = QDir(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation));
    connect(ui->loadBtn, &QPushButton::clicked, this, &MainWindow::load_config_select);
    connect(ui->saveBtn, &QPushButton::clicked, this, &MainWindow::save_config_select);

    // Set icons

    // Mode select
    connect(ui->hostModeSelect, &QRadioButton::clicked, this, &MainWindow::set_host_defaults);
    connect(ui->joinModeSelect, &QRadioButton::clicked, this, &MainWindow::set_join_defaults);
    connect(ui->customModeSelect, &QRadioButton::clicked, this, &MainWindow::set_custom_defaults);
    connect(ui->swapBtn, &QPushButton::clicked, this, &MainWindow::swap_ports);

    // Bridge management
    timer = new QTimer(this);
    timer->setInterval(3000);
    connect(ui->connectBtn, &QPushButton::clicked, this, &MainWindow::create_bridge);
    connect(ui->listWidget, &QListWidget::itemSelectionChanged, this, &MainWindow::update_bridge_status);
    connect(timer, &QTimer::timeout, this, &MainWindow::update_bridge_status);
    connect(ui->killBtn, &QPushButton::clicked, this, &MainWindow::terminate_selected);
    connect(ui->killAllBtn, &QPushButton::clicked, this, &MainWindow::terminate_all);

    // IP
    get_ip_suggestions();
    connect(ui->refreshV6Btn, &QToolButton::clicked, this, &MainWindow::get_ip_suggestions);
    connect(ui->copyV6Btn, &QToolButton::clicked, this, &MainWindow::copy_ipv6);

    // System tray icon
    create_tray_icon();

    timer->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * @brief MainWindow::set_host_defaults
 *
 * Default settings for the person hosting the game server:
 * Listen on given source port of all IPv6 addresses and forward packets to
 * localhost IPv4 game-specific port
 */
void MainWindow::set_host_defaults()
{
    ui->srcAddrInput->setText("::");
    ui->srcAddrInput->setDisabled(true);
    ui->dstAddrInput->setText("127.0.0.1");
    ui->dstAddrInput->setDisabled(true);
}

/**
 * @brief MainWindow::set_join_defaults
 *
 * Default settings for everyone joining the game server:
 * Listen on game-specific port of all IPv4 addresses and forward packets to
 * public IPv6 address:port of host
 */
void MainWindow::set_join_defaults()
{
    ui->srcAddrInput->setText("0.0.0.0");
    ui->srcAddrInput->setDisabled(true);
    ui->dstAddrInput->setDisabled(false);
}

/**
 * @brief MainWindow::set_custom_defaults
 *
 * All fields can be edited freely. No defaults are set.
 */
void MainWindow::set_custom_defaults()
{
    ui->srcAddrInput->setDisabled(false);
    ui->dstAddrInput->setDisabled(false);
}

/**
 * @brief MainWindow::swap_ports
 *
 * Swap source and destination ports
 */
void MainWindow::swap_ports()
{
    quint16 port = ui->srcPortInput->value();
    ui->srcPortInput->setValue(ui->dstPortInput->value());
    ui->dstPortInput->setValue(port);
}

void MainWindow::create_bridge()
{
    // Get addresses and ports from ui
    QHostAddress srcAddr = QHostAddress(ui->srcAddrInput->text());
    QString dstDomain = ui->dstAddrInput->text();
    quint16 srcPort = ui->srcPortInput->value();
    quint16 dstPort = ui->dstPortInput->value();
    // Get user-defined name from UI.
    // Add protocol as postfix if both TCP and UDP are selected
    QString name = ui->nameInput->text();
    // Create UDP and/or TCP bridge based on protocol selection
    bool udp = ui->udpCheckBox->isChecked();
    bool tcp = ui->tcpCheckBox->isChecked();

    QHostInfo::lookupHost(dstDomain, [=](const QHostInfo &result){
        QList<QHostAddress> ipList = result.addresses();
        if (ipList.isEmpty()) return;
        std::sort(ipList.begin(), ipList.end());
        if (udp) {
            UdpBridge *bridge = new UdpBridge(srcAddr, srcPort, ipList[0], dstPort);
            bridge->dstDomain = dstDomain;
            if (!name.isEmpty()) {
                bridge->name = tcp ? name + "_udp" : name;
            }
            start_bridge_thread(bridge);
        }
        if (tcp) {
            TcpBridge *bridge = new TcpBridge(srcAddr, srcPort, ipList[0], dstPort);
            bridge->dstDomain = dstDomain;
            if (!name.isEmpty()) {
                bridge->name = udp ? name + "_tcp" : name;
            }
            start_bridge_thread(bridge);
        }
    });

}

/**
 * @brief MainWindow::start_bridge_thread
 * @param bridge
 *
 * Create a thread for the given bridge and a list item containing related info
 * Connect necessary signals, add the list item to UI list, then start the thread
 */
void MainWindow::start_bridge_thread(BaseBridge *bridge)
{
    BridgeThread *bridgeThread = new BridgeThread(bridge);
    // Create a listWidgetItem containing the bridge information
    BridgeListItem *listItem = new BridgeListItem();
    // The listItem should self-destruct when the connection terminates
    connect(bridgeThread, &QThread::finished, ui->listWidget, [this, listItem]{
        delete ui->listWidget->takeItem(ui->listWidget->row(listItem));});
    listItem->setBThread(bridgeThread);
    ui->listWidget->addItem(listItem);
    bridgeThread->start();
}

/**
 * @brief MainWindow::update_bridge_status
 *
 * Show status of the currently selected bridge
 */
void MainWindow::update_bridge_status()
{
    BridgeListItem *item = (BridgeListItem*)ui->listWidget->currentItem();
    if (item == nullptr) return;
    ui->nameLabel->setText(item->bThread->name);
    ui->srcAddrLabel->setText(item->bThread->srcAddr.toString());
    ui->dstAddrLabel->setText(item->bThread->dstAddr.toString());
    ui->srcPortLabel->setText(QString::number(item->bThread->srcPort));
    ui->dstPortLabel->setText(QString::number(item->bThread->dstPort));
    ui->countLabel->setText(QString::number(item->bThread->connectionCount));
}

/**
 * @brief MainWindow::terminate_selected
 *
 * Terminate the selected bridge
 */
void MainWindow::terminate_selected()
{
    BridgeListItem *item = (BridgeListItem*)ui->listWidget->currentItem();
    if (item == nullptr) return;
    item->bThread->quit();
}

void MainWindow::terminate_all()
{
    int total = ui->listWidget->count();
    for (int i = total-1; i >= 0; i--) {
        BridgeListItem *item = (BridgeListItem*)ui->listWidget->item(i);
        if (item != nullptr) item->bThread->quit();
    }
}

bool MainWindow::load_config(QString fileName, QJsonObject *cfgObj)
{
    QFile cfgFile(fileName);
    if (!cfgFile.exists()) return false;
    if (!cfgFile.open(QFile::ReadOnly)) return false;
    qInfo() << "Loading config file:" << fileName;
    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(cfgFile.readAll(), &error);
    if (error.error != QJsonParseError::NoError) {
        qCritical() << "Cannot parse config file:" << error.errorString();
        return false;
    }
    QJsonObject cfg = doc.object();
    if (cfgObj) *cfgObj = cfg;
    if (cfg.isEmpty()) {
        qCritical() << "Config file contains an empty object";
        return false;
    }
    QJsonArray bridgeArray = cfg.value("bridges").toArray();
    foreach (QJsonValue var, bridgeArray) {
        QJsonObject obj = var.toObject();
        HostIdentity src(obj.value("src").toString());
        HostIdentity dst(obj.value("dst").toString());
        bool tcp = obj.value("tcp").toBool();
        bool udp = obj.value("udp").toBool();
        QString name = obj.value("name").toString();

        QHostInfo::lookupHost(dst.addr, [=](const QHostInfo &result){
            QList<QHostAddress> ipList = result.addresses();
            if (ipList.isEmpty()) return;
            std::sort(ipList.begin(), ipList.end());
            if (tcp) {
                TcpBridge *bridge = new TcpBridge(QHostAddress(src.addr), src.port, ipList[0], dst.port);
                if (!name.isEmpty()) bridge->name = name;
                bridge->dstDomain = dst.addr;
                start_bridge_thread(bridge);
            }
            if (udp) {
                UdpBridge *bridge = new UdpBridge(QHostAddress(src.addr), src.port, ipList[0], dst.port);
                if (!name.isEmpty()) bridge->name = name;
                bridge->dstDomain = dst.addr;
                start_bridge_thread(bridge);
            }
        });
    }

    return true;
}

bool MainWindow::save_config(QString fileName)
{
    int total = ui->listWidget->count();
    QJsonArray cfgArray;
    QJsonObject cfgObj;
    QJsonDocument cfgDoc;
    for (int i = 0; i < total; i++) {
        BridgeListItem *item = (BridgeListItem*)ui->listWidget->item(i);
        if (item != nullptr) {
            cfgArray.push_back(item->getBThread()->bridge->toJson());
        }
    }
    cfgObj.insert("bridges", cfgArray);
    cfgDoc.setObject(cfgObj);
    QFile cfgFile(fileName);
    if (cfgFile.open(QFile::WriteOnly)) {
        cfgFile.write(cfgDoc.toJson());
        return true;
    }
    return false;
}

void MainWindow::load_config_select()
{
    QString fileName = QFileDialog::getOpenFileName(
        this, tr("JSON config file"), cfgDir.path(), "JSON (*.json);;Any (*)");
    load_config(fileName);
}

void MainWindow::save_config_select()
{
    QString fileName = QFileDialog::getSaveFileName(
        this, tr("JSON config file"), cfgDir.filePath("config.json"), "JSON (*.json);;Any (*)");
    save_config(fileName);
}

/**
 * @brief MainWindow::get_ip_suggestions
 *
 * Get all IPs from QNetworkInterface and show the global IPv6 addresses
 */
void MainWindow::get_ip_suggestions()
{
    QList<QHostAddress> allIPs = QNetworkInterface::allAddresses();
    ui->ipv6AddrList->clear();
    foreach (QHostAddress addr, allIPs) {
        if (addr.isGlobal() && addr.protocol() == QAbstractSocket::IPv6Protocol) {
            ui->ipv6AddrList->addItem(addr.toString());
        }
    }
}

/**
 * @brief MainWindow::copy_ipv6
 *
 * Copy the currently selected IPv6 address to clipboard
 */
void MainWindow::copy_ipv6()
{
    clipboard->setText(ui->ipv6AddrList->currentText());
}

void MainWindow::create_tray_icon()
{
    QAction *restoreAction = new QAction(tr("&Show window"), this);
    QAction *quitAction = new QAction(tr("&Quit"), this);
    QMenu *trayIconMenu = new QMenu(this);
    connect(restoreAction, &QAction::triggered, this, &QMainWindow::showNormal);
    connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    QSystemTrayIcon* trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->setIcon(QIcon(":/icons/p2p.svg"));
    trayIcon->show();
}

/**
 * @brief MainWindow::closeEvent
 * @param event
 *
 * If any active bridge is running, hide the window but do not quit the app
 */
void MainWindow::closeEvent(QCloseEvent *event)
{
    if (ui->listWidget->count() > 0) {
        this->hide();
        event->ignore();
    }
}

bool operator<(const QHostAddress &lhs, const QHostAddress &rhs) noexcept {
    // IPv4 is 0 and IPv6 is 1. We want to prioritize IPv6 addresses
    return lhs.protocol() > rhs.protocol();
}
