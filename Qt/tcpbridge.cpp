#include "tcpbridge.h"
#include "tcprelaysocket.h"

TcpBridge::TcpBridge(QHostAddress srcAddr, quint16 srcPort,
                     QHostAddress dstAddr, quint16 dstPort,
                     QObject *parent)
    : BaseBridge{srcAddr, srcPort, dstAddr, dstPort, parent}
{
    name = QString("TCP_%1_%2").arg(QString::number(srcPort), QString::number(dstPort));
}

TcpBridge::~TcpBridge()
{
    qInfo() << QString("Stopped TCP bridge between %1:%2 and %3:%4").arg(
        srcAddr.toString(), QString::number(srcPort), dstAddr.toString(), QString::number(dstPort));
}

bool TcpBridge::run()
{
    // Create a TCP server to manage incoming connections
    tcpServer = new QTcpServer(this);
    connect(tcpServer, &QTcpServer::newConnection, this, &TcpBridge::connect_new);
    if (tcpServer->listen(srcAddr, srcPort)) {
        qInfo() << QString("TCP bridge started between %1:%2 and %3:%4").arg(
            srcAddr.toString(), QString::number(srcPort), dstAddr.toString(), QString::number(dstPort));
        return true;
    } else {
        qCritical() << "Cannot start QTcpServer on port" << srcPort;
        return false;
    }
}

void TcpBridge::connect_new()
{
    while (tcpServer->hasPendingConnections()) {
        // srcSock communicates with source
        // dstSock communicates with destination
        // Both sockets self-destruct on disconnection
        QTcpSocket *srcSock = tcpServer->nextPendingConnection();
        TcpRelaySocket *dstSock = new TcpRelaySocket(srcSock, this);
        connect(dstSock, &QTcpSocket::disconnected, dstSock, &QTcpSocket::deleteLater);
        // Update connection counter on disconnection
        connect(dstSock, &QTcpSocket::disconnected, this, [this]{emit countUpdated(--connectionCount);});
        connectionCount++;
        connect(dstSock, &QAbstractSocket::connected, this, [srcSock, this](){
            qInfo() << QString("TCP connection established between %1:%2 and %3:%4").arg(
                srcSock->peerAddress().toString(), QString::number(srcSock->peerPort()),
                dstAddr.toString(), QString::number(dstPort)
            );
        });
        connect(dstSock, &QAbstractSocket::errorOccurred, this, [this](QAbstractSocket::SocketError error){
            qCritical() << "Cannot connect to" << dstAddr.toString() << "Got error:" << error;
        });
        dstSock->connectToHost(dstAddr, dstPort);
        qInfo() << QString("Establishing TCP connection between %1:%2 and %3:%4").arg(
            srcSock->peerAddress().toString(), QString::number(srcSock->peerPort()),
            dstAddr.toString(), QString::number(dstPort)
        );
    }
    emit countUpdated(connectionCount);

}

QJsonObject TcpBridge::toJson()
{
    QJsonObject obj = BaseBridge::toJson();
    obj.insert("tcp", QJsonValue(true));
    return obj;
}
