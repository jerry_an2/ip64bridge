#ifndef BRIDGELISTITEM_H
#define BRIDGELISTITEM_H

#include <QListWidgetItem>
#include "bridgethread.h"

class BridgeListItem : public QListWidgetItem
{
public:
    BridgeListItem(QListWidget *listview = nullptr, int type = Type);
    BridgeThread *bThread;
    BridgeThread *getBThread() const;
    void setBThread(BridgeThread *newBThread);
};

#endif // BRIDGELISTITEM_H
