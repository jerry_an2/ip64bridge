#include "udprelaysocket.h"
#include <QNetworkDatagram>

UdpRelaySocket::UdpRelaySocket(QUdpSocket *srcSock, QHostAddress srcAddr, quint16 srcPort,
                               QTimer *timer, QObject *parent)
    : QUdpSocket{parent}, srcSock(srcSock), srcAddr(srcAddr), srcPort(srcPort),
    keepAlive(true), activityTimer(timer)
{
    // Whenever this socket receives a datagram, forward it to remote
    connect(this, &QUdpSocket::readyRead, this, &UdpRelaySocket::forward_response);
    if (timer) connect(timer, &QTimer::timeout, this, &UdpRelaySocket::check_liveliness);
}

void UdpRelaySocket::forward_response()
{
    keepAlive = true;
    while (hasPendingDatagrams()) {
        QNetworkDatagram data = receiveDatagram();
        srcSock->writeDatagram(data.data(), srcAddr, srcPort);
    }
    qDebug() << "Forwarding response back to" << srcAddr.toString() << ":" << srcPort
             << "from" << srcSock->localAddress().toString() << ":" << srcSock->localPort();
}

/**
 * @brief UdpRelaySocket::check_liveliness
 *
 * If no activity was observed since the last timeout,
 * the socket should be closed and deleted
 */
void UdpRelaySocket::check_liveliness()
{
    if (not keepAlive) {
        // Emitting the abandoned signal tells parent to release any resource
        // allocated to this address:port
        emit abandoned(srcAddr, srcPort);
    }
    keepAlive = false;
}
