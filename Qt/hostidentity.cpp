#include "hostidentity.h"

HostIdentity::HostIdentity(QString host)
{
    // Some people like to wrap IPs (especially IPv6s) inside square brackets
    host.remove('[').remove(']');
    // A ':' could be the IPv6 deliminator, or the ip:port deliminator,
    // so we take the number after the last ':' and intepret it as port
    QStringList list = host.split(':');
    if (list.size() < 2) {
        port = 0; // This invalidates the object
    } else {
        port = list.takeLast().toUShort(nullptr, 10);
        addr = list.join(':');
    }
}

/**
 * @brief HostIdentity::isValid
 * @return true if both address and port are valid, false otherwise
 */
bool HostIdentity::isValid() const
{
    return !addr.isEmpty() && port != 0;
}

/**
 * @brief HostIdentity::toString
 * @return "invalid_host", "IPv4:port", or "[IPv6]:port"
 */
QString HostIdentity::toString() const
{
    if (!isValid()) return QString("invalid_host");
    return QString("%1:%2").arg(addr, QString::number(port));
}

bool operator==(const HostIdentity &lhs, const HostIdentity &rhs) noexcept {
    return lhs.addr == rhs.addr && lhs.port == rhs.port;
}

size_t qHash(const HostIdentity &c, size_t seed) noexcept {
    return qHashMulti(seed, c.addr, c.port);
}
