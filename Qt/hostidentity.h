#ifndef HOSTIDENTITY_H
#define HOSTIDENTITY_H

#include <QHostAddress>

/**
 * @brief The HostIdentity class
 *
 * Contains an address and a port. We will use instances of this class as keys
 * to QHash dictionaries storing socket pointers. Note that this would require
 * defining the comparison operator (==) and qHash function for this class
 */
struct HostIdentity {
    QString addr;
    quint16 port;
    HostIdentity(const QString &addr, quint16 port) : addr(addr), port(port) {}
    HostIdentity(QString host); // Not const reference because modification needed
    bool isValid() const;
    QString toString() const;
};

bool operator==(const HostIdentity &lhs, const HostIdentity &rhs) noexcept;

size_t qHash(const HostIdentity &c, size_t seed) noexcept;

#endif // HOSTIDENTITY_H
