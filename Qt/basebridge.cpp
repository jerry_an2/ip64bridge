#include "basebridge.h"
#include "hostidentity.h"

BaseBridge::BaseBridge(QHostAddress srcAddr, quint16 srcPort,
                       QHostAddress dstAddr, quint16 dstPort,
                       QObject *parent)
    : QObject{parent}, connectionCount(0),
    srcAddr(srcAddr), srcPort(srcPort), dstAddr(dstAddr), dstPort(dstPort)
{
    // destination domain name defaults to destination IP.
    // If desired, overwrite this value after object creation
    dstDomain = dstAddr.toString();
}

QJsonObject BaseBridge::toJson()
{
    HostIdentity src(srcAddr.toString(), srcPort);
    HostIdentity dst(dstDomain, dstPort);
    return QJsonObject({
        JsonPair("name", QJsonValue(name)),
        JsonPair("src", QJsonValue(src.toString())),
        JsonPair("dst", QJsonValue(dst.toString())),
        JsonPair("tcp", QJsonValue(false)),
        JsonPair("udp", QJsonValue(false)),
    });
}
