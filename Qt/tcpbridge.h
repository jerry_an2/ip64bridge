#ifndef TCPBRIDGE_H
#define TCPBRIDGE_H

#include <QTcpServer>
#include <QTcpSocket>
#include "basebridge.h"

class TcpBridge : public BaseBridge
{
    Q_OBJECT
public:
    QTcpServer *tcpServer;
    explicit TcpBridge(QHostAddress srcAddr, quint16 srcPort,
                       QHostAddress dstAddr,quint16 dstPort,
                       QObject *parent = nullptr);
    ~TcpBridge();
    virtual void connect_new();

    // BaseBridge interface
public:
    virtual bool run();
    QJsonObject toJson();
};

#endif // TCPBRIDGE_H
