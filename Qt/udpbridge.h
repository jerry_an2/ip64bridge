#ifndef UDPBRIDGE_H
#define UDPBRIDGE_H

#include <QUdpSocket>
#include <QHash>
#include <QTimer>
#include "hostidentity.h"
#include "basebridge.h"
#include "udprelaysocket.h"

class UdpBridge : public BaseBridge
{
    Q_OBJECT
public:
    QHash<HostIdentity, UdpRelaySocket*> relaySockets;
    QUdpSocket *srcSock;
    QTimer *timer; // Liveliness timer
    explicit UdpBridge(QHostAddress srcAddr, quint16 srcPort,
                       QHostAddress dstAddr,quint16 dstPort,
                       QObject *parent = nullptr);
    ~UdpBridge();
    virtual void forward_data();
    virtual void remove_socket(QHostAddress srcAddr, quint16 srcPort);
    virtual void report_status();

    // BaseBridge interface
public:
    virtual bool run();
    QJsonObject toJson();
};

#endif // UDPBRIDGE_H
