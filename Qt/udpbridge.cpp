#include "udpbridge.h"
#include <QNetworkDatagram>

UdpBridge::UdpBridge(QHostAddress srcAddr, quint16 srcPort,
                     QHostAddress dstAddr, quint16 dstPort,
                     QObject *parent)
    : BaseBridge{srcAddr, srcPort, dstAddr, dstPort, parent}
{
    name = QString("UDP_%1_%2").arg(QString::number(srcPort), QString::number(dstPort));
}

UdpBridge::~UdpBridge()
{
    qInfo() << QString("Stopped UDP bridge between %1:%2 and %3:%4").arg(
        srcAddr.toString(), QString::number(srcPort), dstAddr.toString(), QString::number(dstPort));
    // All members are children of this object. No need for manual deletion
}

bool UdpBridge::run()
{
    timer = new QTimer(this);
    timer->setInterval(10000);
    timer->start();
    srcSock = new QUdpSocket(this);
    connect(srcSock, &QUdpSocket::readyRead, this, &UdpBridge::forward_data);
    connect(timer, &QTimer::timeout, this, &UdpBridge::report_status);
    if (srcSock->bind(srcAddr, srcPort)) {
        qInfo() << QString("UDP bridge started between %1:%2 and %3:%4").arg(
            srcAddr.toString(), QString::number(srcPort), dstAddr.toString(), QString::number(dstPort));
        return true;
    } else {
        qCritical() << "Cannot bind QUdpSocket to port" << srcPort;
        return false;
    }
}

QJsonObject UdpBridge::toJson()
{
    QJsonObject obj = BaseBridge::toJson();
    obj.insert("udp", QJsonValue(true));
    return obj;
}

void UdpBridge::forward_data()
{
    UdpRelaySocket *relaySock;
    QNetworkDatagram data = srcSock->receiveDatagram();
    QHostAddress sender = data.senderAddress();
    HostIdentity host = HostIdentity(sender.toString(), data.senderPort());
    qDebug() << "Received data from host" << srcAddr << ":" << srcPort;
    if (not relaySockets.contains(host)) {
        relaySock = new UdpRelaySocket(srcSock, sender, host.port, timer, this);
        relaySockets.insert(host, relaySock);
        connect(relaySock, &UdpRelaySocket::abandoned, this, &UdpBridge::remove_socket);
        qInfo() << "Created new bridge for host" << srcAddr << ":" << srcPort;
        emit countUpdated(++connectionCount);
    }
    relaySock = relaySockets.value(host);
    relaySock->writeDatagram(data.data(), dstAddr, dstPort);
    relaySock->keepAlive = true;
}

void UdpBridge::remove_socket(QHostAddress srcAddr, quint16 srcPort)
{
    HostIdentity host = HostIdentity(srcAddr.toString(), srcPort);
    delete relaySockets.take(host);
    qInfo() << "Removed inactive host" << srcAddr << ":" << srcPort;
}

void UdpBridge::report_status()
{
    qDebug() << "The bridge contains" << relaySockets.size() << "alive connections";
    if (connectionCount != relaySockets.size()) {
        connectionCount = relaySockets.size();
        emit countUpdated(connectionCount);
    }
}
