#include "bridgethread.h"

int BridgeThread::getConnectionCount() const
{
    return connectionCount;
}

void BridgeThread::setConnectionCount(int newConnectionCount)
{
    connectionCount = newConnectionCount;
}

BridgeThread::BridgeThread(BaseBridge *bridge, QObject *parent)
    : QThread{parent}, bridge(bridge), connectionCount(0)
{
    // Copy over all the info fields so they can be safely accessed by UI
    srcAddr = bridge->srcAddr;
    srcPort = bridge->srcPort;
    dstAddr = bridge->dstAddr;
    dstPort = bridge->dstPort;
    name = bridge->name;
    bridge->moveToThread(this);
    connect(this, &QThread::finished, this, &QThread::deleteLater);
}

void BridgeThread::run()
{
    // Start the event loop only if sockets are bound successfully
    connect(bridge, &BaseBridge::countUpdated, this, &BridgeThread::setConnectionCount);
    if (bridge->run()) exec();
    bridge->deleteLater();
}
