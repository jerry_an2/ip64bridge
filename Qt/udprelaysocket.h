#ifndef UDPRELAYSOCKET_H
#define UDPRELAYSOCKET_H

#include <QUdpSocket>
#include <QHostAddress>
#include <QTimer>

/**
 * @brief The UdpRelaySocket class
 *
 * Each instance of this class is tied to a source address:port and handles
 * all the response forwarding.
 */
class UdpRelaySocket : public QUdpSocket
{
    Q_OBJECT
public:
    // Response should return to srcAddr:srcPort via srcSock
    QUdpSocket *srcSock;
    QHostAddress srcAddr;
    quint16 srcPort;
    bool keepAlive;
    QTimer *activityTimer;

    explicit UdpRelaySocket(QUdpSocket *srcSock, QHostAddress srcAddr, quint16 srcPort,
                            QTimer *timer, QObject *parent = nullptr);
    virtual void forward_response();
    virtual void check_liveliness();

signals:
    void abandoned(QHostAddress addr, quint16 port);
};

#endif // UDPRELAYSOCKET_H
