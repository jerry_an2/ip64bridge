#ifndef BASEBRIDGE_H
#define BASEBRIDGE_H

#include <QObject>
#include <QHostAddress>
#include <QJsonObject>

typedef QPair<QString, QJsonValue> JsonPair;

class BaseBridge : public QObject
{
    Q_OBJECT
public:
    int connectionCount;
    QHostAddress srcAddr;
    quint16 srcPort;
    QHostAddress dstAddr;
    quint16 dstPort;
    QString name;
    QString dstDomain;
    explicit BaseBridge(QHostAddress srcAddr, quint16 srcPort,
                        QHostAddress dstAddr,quint16 dstPort,
                        QObject *parent = nullptr);

    virtual bool run() = 0;
    virtual QJsonObject toJson();
signals:
    void countUpdated(int count);
};

#endif // BASEBRIDGE_H
