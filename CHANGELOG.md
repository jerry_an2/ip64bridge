# 1.0.0 DNS Lookup

* [Qt] Add support for domain name as destination
* [Qt] Always ignore system proxy because it might cause problems for this app.

# 0.3.0 JSON config support

* Support for config file in json format
* [Qt] Fix app random crash on terminating a bridge

# 0.2.0 QoL updates

* [Qt] Add headless (non-gui) mode
* [Qt] Add system tray icon
* [Python] Add command-line argument parser
* [Python] Performance optimizations and bug fixes

# 0.1.0 Initial release
